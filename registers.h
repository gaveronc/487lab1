/*
* This file defines a selection of useful registers such as
* clock registers and GPIO registers. It is likely that this
* file will be expanded as time goes on. Currently, the
* registers are all treated as pointers and need to be
* dereferenced accordingly in the main program when used.
*/

#ifndef REGISTERS_H
#define REGISTERS_H
#include "util.h"

#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB2ENR           ((volatile uint32_t *)(RCC_BASE + 0x18))
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR             ((volatile uint32_t *)(GPIOB_BASE + 0x0C))
#define GPIOB_CRH             ((volatile uint32_t *)(GPIOB_BASE + 0x04))
#define GPIOB_BSRR            ((volatile uint32_t *)(GPIOB_BASE  + 0x10))
#define GPIOB_BRR             ((volatile uint32_t *)(GPIOB_BASE  + 0x14))

#endif      
