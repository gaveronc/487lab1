/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Lab 1
*/

#define LED_NUM 8 //Board has 8 LEDs

#include <stdio.h>
#include "util.h"
#include "registers.h"

//Dirty delay loop
void delay(unsigned count) {
	int i;
	for (i = 0; i < count; i++) {
	}
}

/*
* This program is the "Hello, World" of microcontroller boards;
* that is to say all it does is enable a bank of LEDs and the
* clocks associated with those LEDs, then makes them blink in
* a novel way. This particular program makes the LEDs blink in
* a way similar to a cylon from Battlestar Galactica, which is
* to say the LEDs "move" from left to right.
*/

int main()
{
	//Need to setup the the LED's on the board
	*RCC_APB2ENR |= 0x08; // Enable Port B clock
	*GPIOB_ODR  &= ~0x0000FF00; // Switch off LEDs
	*GPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
  
	*GPIOB_ODR = 1 << 8; // Turn on first LED
	delay(1000000); // I don't know how long this is, but I will delay for 1000000 of it
	
	while (1) {
		//Count up
		int i;
		for (i = 0; i < LED_NUM - 1; i = i + 1) {
			*GPIOB_ODR = 1 << (9 + i);
			delay(1000000);
		}
		//Count down
		for (i = 0; i < LED_NUM - 1; i = i + 1) {
			*GPIOB_ODR = 1 << (14 - i);
			delay(1000000);
		}
	}
}
